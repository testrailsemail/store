class CreateCategories < ActiveRecord::Migration[5.2]
  def change
    create_table :categories do |t|
      t.bigint :parent_id
      t.string :name
      t.integer :sort_position

      t.timestamps
    end
  end
end
