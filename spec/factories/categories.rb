# == Schema Information
#
# Table name: categories
#
#  id            :bigint(8)        not null, primary key
#  ancestry      :string
#  name          :string
#  sort_position :integer
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#  parent_id     :bigint(8)
#
# Indexes
#
#  index_categories_on_ancestry  (ancestry)
#

FactoryBot.define do
  factory :category do
    name { Faker::Lorem.words(1).join(' ') }
    sort_position { rand(1..10) }
  end
end
