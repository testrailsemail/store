# == Schema Information
#
# Table name: products
#
#  id          :bigint(8)        not null, primary key
#  image       :string
#  name        :string
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  category_id :bigint(8)
#
# Indexes
#
#  index_products_on_category_id  (category_id)
#
# Foreign Keys
#
#  fk_rails_...  (category_id => categories.id)
#

FactoryBot.define do
  factory :product do
    name { Faker::Lorem.words(1).join(' ') }
  end
end
