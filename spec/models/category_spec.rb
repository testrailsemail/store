# == Schema Information
#
# Table name: categories
#
#  id            :bigint(8)        not null, primary key
#  ancestry      :string
#  name          :string
#  sort_position :integer
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#  parent_id     :bigint(8)
#
# Indexes
#
#  index_categories_on_ancestry  (ancestry)
#

require 'rails_helper'

RSpec.describe Category, type: :model do
  describe 'category' do
    it 'has valid factory' do
      expect { create(:category) }.to change { Category.count }.by(1)
    end
  end

  describe 'validations' do
    it { should validate_presence_of(:name) }
    it { should validate_uniqueness_of(:name).case_insensitive }
    it { should validate_presence_of(:sort_position) }
  end

  describe 'associations' do
    it { should have_many(:products).dependent(:nullify) }
  end
end
