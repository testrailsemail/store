# == Schema Information
#
# Table name: products
#
#  id          :bigint(8)        not null, primary key
#  image       :string
#  name        :string
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  category_id :bigint(8)
#
# Indexes
#
#  index_products_on_category_id  (category_id)
#
# Foreign Keys
#
#  fk_rails_...  (category_id => categories.id)
#

require 'rails_helper'

RSpec.describe Product, type: :model do
  describe 'product' do
    it 'has valid factory' do
      expect { create(:product, category: build(:category)) }.to change { Product.count }.by(1)
    end
  end

  describe 'validations' do
    it { should validate_presence_of(:name) }
    it { should validate_uniqueness_of(:name).case_insensitive }
  end

  describe 'associations' do
    it { should belong_to(:category) }
  end
end
